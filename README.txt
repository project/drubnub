DrubNub module:
---------------
Author - Matt Vance (mvance at pobox dot com)
Requires - Drupal 6
License - GPL

Overview:
--------

The DrubNub module provides functionality similar to YubNub.org. The module adds a new "DrubNub Command" content type that users can use to create search shortcuts. Commands can then be entered into the DrubNub "command line" form along with search terms and DrubNub will then redirect the user to the search results page on the site designated by the command.

For example, a user can enter "g [search terms]" to perform a search on Google.com. Entering "g Drupal" into the DrubNub command line will redirect the user to the Google.com results page for a search of the term Drupal. 

Installation:
------------

1) Download the module files from the project page on Drupal.org: http://drupal.org/project/drubnub

2) DrubNub requires that the Views module be enabled, in order for the command list ("ls" command) to display. If you do not already have the Views module installed, it can be downloaded from the Views project page on Drupal.org: http://drupal.org/project/views

3) Decompress and copy the files into their respective subdirectories under your site's modules directory (such as /sites/all/modules).

4) Enable the module(s) on the Modules page (Administer > Site Building > Modules).

5) Enable the DrubNub Command Line block on the Blocks page (Administer > Site Building > Blocks).

Use:
----

DrubNub installs several commands by default.

> ls
The 'ls' command redirects the user to a listing of available DrubNub commands.

> create
The 'create' command redirects users to a page for creating new DrubNub commands.

> man [command]
The 'man' command redirects users to a page that provides details about a specific command.

> g [search terms]
The 'g' command performs a search using the Google search engine.

> y [search terms]
The 'y' command performs a search using the Yahoo! search engine.

Tips & Tricks:
--------------

1) DrubNub uses the same syntax used by most basic YubNub.org commands. To add new commands quickly, find a corresponding command on YubNub to enter into DrubNub. (NOTE: Some advanced YubNub features are not yet supported, such as multiple parameters, default values, and converting GET to POST.) 

2) Entering a search command without any search terms will redirect the user to the home page associated with the command. For instance, entering 'g' will redirect the users to http://www.google.com/.

3) By default, when a command is not found, the user will be redirected to the command list with an error message explaining that the command was not found. Alternatively, DrubNub can be configured to pass search terms on to a specific command instead, by specifying a default search on the DrubNub administration page (Administer > Site configuration > DrubNub).

Specifying a general purpose search engine command, such as a Google search, allows users an even quicker shortcut to Google, as long as their search terms do not start with a term corresponding to an existing DrubNub command.

DrubNub can even leverage the hundreds of existing YubNub.org commands by specifying a YubNub search as the default.

4) By default, DrubNub uses a plus sign (+) to separate search terms when generating redirect URLs. However, some sites require an alternative separator in search URLs. When creating or editing a command, users can specify an alternative substitution by adding "[use X for spaces]" (where "X" is the character you want substituted) to the end of a URL. For example, add "[use - for spaces]" to specify dashes as the separator for a particular command. Use "%20" to designate a blank space as a separator.

Integration:
------------

DrubNub is most useful when integrated with a desktop launcher utility such as Quicksilver or Launchy. The URL needed to integrate DrubNub with third party launchers can be found on the administration page (Administer > Site configuration > DrubNub).

For a list of recommended launcher utilities for a variety of operating systems, see Lifehacker.com's "Best Application Launchers" article: http://lifehacker.com/392569/best-application-launchers

DrubNub can be used in much the same way as YubNub.org. An extensive list of methods for integrating YubNub with third-party software is available at the following URL: http://yubnub.org/documentation/describe_installation

Last updated:
------------
