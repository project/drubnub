<?php

function drubnub_admin_settings(&$form_state) {
  global $base_url;
  $parse_url = $base_url ."/drubnub/parse?command=[search terms]";
  $form['drubnub']['redirect_url'] = array(
    '#value' => t('Use the following URL to integrate DrubNub with external launcher programs:<br> '. $parse_url),
  );
  $form['drubnub']['drubnub_default_search'] = array(
    '#type' => 'textfield',
    '#title' => t('DrubNub default search'),
    '#description' => t('Enter the name of a command to fall back to when a user\'s search string that does not contain an existing DrubNub command.'),
    '#default_value' => variable_get('drubnub_default_search', ''),
    '#size' => 32,
    '#maxlength' => 32,
  );
  return system_settings_form($form);  
}

function drubnub_import_commands(&$form_state) {
  $form[] = array(
    '#type' => 'markup',
    '#value' => t('<strong>WARNING:</strong> The command import functionality is particularly experimental. <strong>Use at your own risk!</strong>'),
  );
  $form['csv_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload a CVS file to import'),
    '#description' => t('Files should be in CSV format with the fields in the following order: Command Name, Description, URL.'), 
  );
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Import Commands'),
  );  
  return $form;
}

function drubnub_import_commands_validate($form, &$form_state) {
  if ($file = file_save_upload('csv_upload')) {
    $path = $file->filepath;
    
    if ($handle = fopen($path, "r")) {
      $num = 0;
  
      // Read one line at a time and create each command.
      while ($line = fgetcsv($handle)) {
        if ($line) {

          $command = new stdClass;
          $command->uid = 1;
          $command->title = $line['0'];
          $command->body = $line['1'];
          $command->url = $line['2'];
          $command->type = 'drubnub';
          node_save($command);

          $num++;
        }
      }
      // Close the file with fclose/gzclose.
      fclose($handle);
  
      // Delete the file if it is temporary.
      unlink($path);
  
      $message = t("Import complete. %num commands imported.", array("%num" => $num));
      drupal_set_message($message);
    }  
  }
}