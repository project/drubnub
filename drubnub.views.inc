<?php

/**
 * @file
 */

/**
 * Implementation of hook_views_data_alter().
 * 
 */
function drubnub_views_data() {
  // ...
  $data['drubnub']['table']['group']  = t('Node');
  $data['drubnub']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['drubnub']['url'] = array(
    'title' => t('DrubNub URL'),
    'help' => t('The URL of a DrubNub redirect shortcut.'),
    'field' => array(
      'field' => 'url',
      'group' => t('DrubNub'),
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
  );
  
  $data['drubnub']['uses'] = array(
    'title' => t('DrubNub usage count'),
    'help' => t('The number of times a command has been used.'),
    'field' => array(
      'field' => 'uses',
      'group' => t('DrubNub'),
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
  );
  // ...
  return $data;
} 
